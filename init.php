<?php defined('SYSPATH') or die('No direct script access.');

/**
 * PSR-0 autoloader implementation.
 */
spl_autoload_register(function($classname)
{
    if ($classname[0] == '\\')
    {
        $classname = substr($classname, 1);
    }

    $filename  = '';
    $namespace = '';

    if ($last_slash = strripos($classname, '\\'))
    {
        $namespace = substr($classname, 0, $last_slash);
        $classname = substr($classname, $last_slash + 1);
        $filename  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $filename .= str_replace('_', DIRECTORY_SEPARATOR, $classname);
    $filename = Kohana::find_file('vendor/stomp-php/src', $filename);

    if (@is_file($filename))
    {
        require $filename;
        return true;
    }
});